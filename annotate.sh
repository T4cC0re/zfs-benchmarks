test -z "${1}" && exit 1
test -z "${2}" && exit 1
test -z "${3}" && exit 1

source ./.grafana.env

cat <<EOF | curl -H "Authorization: Bearer $TOKEN" -H "Content-Type: application/json" -iXPOST -d @- "${HOST}/api/annotations"
{
  "time":${1}000,
  "isRegion":true,
  "timeEnd":${2}000,
  "tags":["benchmark"],
  "text":"$3"
}
EOF

echo
