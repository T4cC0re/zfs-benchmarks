#!/usr/bin/env bash

TYPE=$1
DISKS=$2
LOCAL_DISKS=$3
MODE=$4
DISKTYPE=$5

START="$(date +%s)"
./create-storage-node.sh "${TYPE}" "${DISKS}" "${LOCAL_DISKS}" "${MODE}" "${DISKTYPE}"
NAME="candicate-zfs-storage-${MODE}-${DISKS}${DISKTYPE}-${LOCAL_DISKS}local-${TYPE}"

sleep 60
./setL2ARC.sh "${NAME}" 2
./setTunables_storage.sh "${NAME}" on
sleep 240
./bench_gitaly.sh "${NAME}" '750GB l2arc, no slog, l2arc tuning, all defaults'

sleep 30
END="$(date +%s)"

URL="http://34.73.202.158:3000/d/Hy1VA2Ciz/overview?orgId=1&from=${START}000&to=${END}000&var-Host=${NAME}:9100"

gcloud compute instances delete --project t4cc0re --zone us-east1-b --quiet "${NAME}"

echo "${URL}"
