#!/usr/bin/env bash

gcloud compute ssh --project=t4cc0re --zone us-east1-b $1 <<EOF
if [ "$2" = "1" ]; then
  sudo zpool status data | grep -q nvme0n1 || sudo zpool add data cache nvme0n1
  sudo zpool status data | grep -q nvme0n2 && sudo zpool remove data nvme0n2
elif [ "$2" = "2" ]; then
  sudo zpool status data | grep -q nvme0n1 || sudo zpool add data cache nvme0n1
  sudo zpool status data | grep -q nvme0n2 || sudo zpool add data cache nvme0n2
else
  sudo zpool status data | grep -q nvme0n1 && sudo zpool remove data nvme0n1
  sudo zpool status data | grep -q nvme0n2 && sudo zpool remove data nvme0n2
fi
sudo zpool status data
EOF
