#!/usr/bin/env bash
test -z "${1}" && exit 1
test -z "${2}" && exit 1

NODE="$1"
DESCRIPTION="$2"

# Usage: runBenchmark <gitaly benchmark> <repo count> <batch size> [<Annotation message>]
runBenchmark() {
  local BENCHMARK="$1"
  local REPOS="$2"
  local BATCH="$3"
  local ANNOTATION_MSG="$4"
  TIME_START=$(date +%s)
  for ((i=1;i<=${REPOS};i++)); do echo root/linux-${i}.git; done | gcloud compute ssh --project=t4cc0re --zone=us-east1-b ${NODE} -- xargs -rn1 -P ${BATCH} -I% sudo ./gitaly-bench --host unix:///var/opt/gitlab/gitaly/gitaly.socket -repo % -concurrency 1 -iterations 1 ${BENCHMARK} # 1>/dev/null
  TIME_END=$(date +%s)
  ./annotate.sh $TIME_START $TIME_END "${NODE} - ${DESCRIPTION} - gitaly linux.git ${REPOS}x${BENCHMARK}, ${BATCH} batch ${ANNOTATION_MSG}"
}

cooldown() {
  echo "Cooling down for $1 sec."
  sleep $1
}

runBenchmark repack-full 250 32 "run 1/2"
cooldown 30
runBenchmark repack-full 250 32 "run 2/2"
cooldown 30
runBenchmark repack-full 250 64 "run 1/2"
cooldown 30
runBenchmark repack-full 250 64 "run 2/2"
