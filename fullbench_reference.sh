#!/usr/bin/env bash

TYPE=$1
DISKS=$2
LOCAL_DISKS=$3
MODE=$4

START="$(date +%s)"
./create-storage-reference-node.sh
NAME="candicate-reference-n1-standard-32"
sleep 60
./bench_gitaly.sh "candicate-reference-n1-standard-32" 'reference like a storage node on gitlab.com'
sleep 30
END="$(date +%s)"

URL="http://34.73.202.158:3000/d/Hy1VA2Ciz/overview?orgId=1&from=${START}000&to=${END}000&var-Host=${NAME}:9100"

gcloud compute instances delete --project t4cc0re --zone us-east1-b --quiet "${NAME}"
echo "${URL}"
