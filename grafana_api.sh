source ./.grafana.env

URL="$1"
shift

curl -H "Authorization: Bearer $TOKEN" -H "Content-Type: application/json" $@ "${HOST}${URL}"
