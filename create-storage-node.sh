#!/usr/bin/env bash

# 2x 375GB local NVMe SSD for ZIL and L2ARC (only available in 1 size)
# 10x 2TB network SSD for ZFS raidz2 pool (8n data, 2n parity) (max provisoined IOPS & thoughput, can be scaled up to 64TB/disk in size w/o change in perf)

TYPE=$1
DISKS=$2
LOCAL_DISKS=$3
export MODE=$4
export DISKTYPE=$5

export NAME="candicate-zfs-storage-${MODE}-${DISKS}${DISKTYPE}-${LOCAL_DISKS}local-${TYPE}"

echo "Creating $NAME"

COMMANDLINE="gcloud beta compute --project=t4cc0re instances create ${NAME} --zone=us-east1-b --machine-type=${TYPE} --subnet=default --network-tier=PREMIUM --maintenance-policy=MIGRATE --service-account=473940073259-compute@developer.gserviceaccount.com --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append --image=ubuntu-1604-xenial-v20190530c --image-project=ubuntu-os-cloud --boot-disk-size=32GB --boot-disk-type=pd-ssd --boot-disk-device-name=${NAME}-boot --tags=http-server,https-server"

if [ "$DISKTYPE" == "hdd" ]; then
  for ((i=1;i<=${DISKS};i++)); do
    COMMANDLINE="${COMMANDLINE} --create-disk=mode=rw,auto-delete=yes,size=2048,type=projects/t4cc0re/zones/us-east1-b/diskTypes/pd-standard,name=${NAME}-zfs-${i}"
  done
else
  for ((i=1;i<=${DISKS};i++)); do
    COMMANDLINE="${COMMANDLINE} --create-disk=mode=rw,auto-delete=yes,size=2048,type=projects/t4cc0re/zones/us-east1-b/diskTypes/pd-ssd,name=${NAME}-zfs-${i}"
  done
fi

for ((i=1;i<=${LOCAL_DISKS};i++)); do
COMMANDLINE="${COMMANDLINE} --local-ssd=interface=NVME"
done

$COMMANDLINE

sleep 30

./install.sh ${NAME} ${MODE}
