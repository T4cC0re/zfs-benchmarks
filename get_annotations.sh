#!/usr/bin/env bash

FILTER="$1"
TMPFILE=$(mktemp)

echo "| Time | Name| Configuration | Benchmark |"
echo "| :--- | :---| :------------ | :-------- |"

render() {
  local TMPF=$(mktemp)
  cat > $TMPF

  local ID=$(jq -r .regionId "$TMPF" | uniq)
  local TEXT=$(jq -r .text "$TMPF" | uniq | sed 's/ - / | /g' )
  local TIME=$(jq -r .time $TMPF)
  readarray -t TIMES <<<"$TIME"

  echo "| $(( $(( ${TIMES[0]} - ${TIMES[1]} )) / 1000)) sec. | $TEXT |"
  rm $TMPF
}

./grafana_api.sh "/api/annotations?tags=benchmark&limit=10000&from=1552485524170" | tee $TMPFILE | jq ".[] | select(.text|test(\"$FILTER\")) | .regionId" | uniq | for i in $(cat -); do jq ".[] | select(.regionId == $i)" $TMPFILE | render; done | sort -Vu

rm ${TMPFILE}
