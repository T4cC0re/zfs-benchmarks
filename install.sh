#!/usr/bin/env bash

IP=$(gcloud compute instances describe --project=t4cc0re --zone=us-east1-b --format=json $1 | jq -r '.networkInterfaces[].accessConfigs[].natIP')
MODE=$2

gcloud compute --project=t4cc0re firewall-rules create default-allow-http --direction=INGRESS --priority=1000 --network=default --action=ALLOW --rules=tcp:80 --source-ranges=0.0.0.0/0 --target-tags=http-server
gcloud compute --project=t4cc0re firewall-rules create default-allow-https --direction=INGRESS --priority=1000 --network=default --action=ALLOW --rules=tcp:443 --source-ranges=0.0.0.0/0 --target-tags=https-server

echo "Add to prometheus"
gcloud compute ssh --project=t4cc0re --zone us-east1-b metrics <<EOF
cat <<CAT | sudo tee -a /etc/prometheus/prometheus.yml
      - targets: ['${IP}:9100']
        labels:
          instance: $1:9100
          alias: $1:9100
CAT
sudo systemctl reload prometheus
EOF

GL_KERNEL_VERSION="4.15.0-1033"

APT_FLAGS='-o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold"'
PACKAGES='zfsutils-linux curl openssh-server ca-certificates bonnie++ ioping fio'
ADMIN_PASS="$(pwgen 32 1)"

echo "Upgrade and install some packages kernel $GL_KERNEL_VERSION"
gcloud compute ssh --project=t4cc0re --zone us-east1-b $1 <<EOF
sudo apt update && sudo DEBIAN_FRONTEND=noninteractive apt ${APT_FLAGS} full-upgrade -y && sudo DEBIAN_FRONTEND=noninteractive apt ${APT_FLAGS} install -y linux-image-${GL_KERNEL_VERSION}-gcp linux-modules-extra-${GL_KERNEL_VERSION}-gcp ${PACKAGES}
sudo reboot
EOF

echo "Wait for machine to be rebooted"
sleep 30
while ! gcloud compute ssh --project=t4cc0re --zone us-east1-b $1 -- exit; do
  sleep 5;
done

if [ "$3" == "database" ]; then
  echo "Setup Database ZFS"
  gcloud compute ssh --project=t4cc0re --zone us-east1-b $1 <<EOF
ls -l /dev/disk/by-id/google-persistent-disk-*
sudo mkdir -p /var/opt/gitlab
sudo zpool create -f -o ashift=12 psql ${MODE} \$(ls /dev/disk/by-id/google-persistent-disk-* | grep -v 'part')
sudo zpool status
sudo zfs set mountpoint=/var/opt/gitlab/postgresql psql
sudo zfs create psql/data
EOF
else

  if [ "$MODE" == "ext4" ]; then
    echo "Setup ext4"
    gcloud compute ssh --project=t4cc0re --zone us-east1-b $1 <<EOF
sudo mkfs.ext4 /dev/disk/by-id/google-persistent-disk-1
sudo mkdir -p /var/opt/gitlab/git-data/repositories
echo "/dev/disk/by-id/google-persistent-disk-1 /var/opt/gitlab/git-data/repositories ext4 discard,defaults 0 2" | sudo tee -a /etc/fstab
sudo mount -a
mount | grep gitlab
sudo mkdir /data
EOF
  else
    echo "Setup ZFS"
    gcloud compute ssh --project=t4cc0re --zone us-east1-b $1 <<EOF
ls -l /dev/disk/by-id/google-persistent-disk-*
sudo zpool create -f -o ashift=12 data ${MODE} \$(ls /dev/disk/by-id/google-persistent-disk-* | grep -v 'part')
sudo zpool status
sudo mkdir -p /var/opt/gitlab/git-data
sudo zfs create -o mountpoint=/var/opt/gitlab/git-data/repositories data/gitaly
EOF
  fi
fi

echo "Install GitLab"
gcloud compute ssh --project=t4cc0re --zone us-east1-b $1 <<EOF
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
sudo EXTERNAL_URL="http://${IP}" apt-get install gitlab-ee
## Gitaly runs on unix:/var/opt/gitlab/gitaly/gitaly.socket
EOF
if [ "$3" == "database" ]; then
  echo "Setup Database ZFS"
  gcloud compute ssh --project=t4cc0re --zone us-east1-b $1 -- sudo bash <<\EOF
gitlab-ctl stop postgresql
cd /var/opt/gitlab/postgresql/data
mv base{,-old}
mv pg_xlog{,-old}
zfs create -o recordsize=8k -o redundant_metadata=most -o primarycache=metadata -o logbias=throughput psql/data/pg_xlog
zfs create -o recordsize=8k -o redundant_metadata=most -o primarycache=metadata -o logbias=throughput psql/data/base
cp -rvp base-old/* base
cp -rvp pg_xlog-old/* pg_xlog
chmod -R 700 base
chmod -R 700 pg_xlog
chown -R gitlab-psql:root base
chown -R gitlab-psql:root pg_xlog
rm -rf base-old pg_xlog-old
gitlab-ctl start postgresql
sleep 15
EOF
fi
echo "Set password for admin user"
gcloud compute ssh --project=t4cc0re --zone us-east1-b $1 -- sudo gitlab-rails console production <<EOF
user = User.where(id: 1).first
user.password = '${ADMIN_PASS}'
user.password_confirmation = '${ADMIN_PASS}'
user.save!
EOF

echo "Setup gitlab to allow scraping of metrics"
gcloud compute ssh --project=t4cc0re --zone us-east1-b $1 <<EOF
cat <<CAT | sudo tee -a /etc/gitlab/gitlab.rb
  node_exporter['listen_address'] = '0.0.0.0:9100'
  postgres_exporter['listen_address'] = '0.0.0.0:9187'
CAT
sudo gitlab-ctl reconfigure
cat > .netrc <<NET
machine ${IP}
login root
password ${ADMIN_PASS}
NET
sleep 60
EOF

if [ "$3" == "database" ]; then
 echo "Setup PSQL benchmarking"
  gcloud compute ssh --project=t4cc0re --zone us-east1-b $1 <<EOF
true # TODO
EOF
else
  echo "Checkout benchmarking repos"
  gcloud compute ssh --project=t4cc0re --zone us-east1-b $1 <<EOF
sudo rm -rf /data/linux.git
sudo git clone --mirror https://gitlab.com/T4cC0re/linux-snapshot.git /data/linux.git
cd /data/linux.git
sudo git push --set-upstream http://${IP}/root/linux.git --all
EOF
  sleep 20
  echo "'Fork' linux repo for gitaly benchmark"
  gcloud compute ssh --project=t4cc0re --zone us-east1-b $1 <<\EOF
for i in {1..250..1}; do echo "${i}/250"; sudo cp -rp /var/opt/gitlab/git-data/repositories/root/linux{,-$i}.git; done
wait
EOF

  echo "Compile and SCP gitaly-bench to machine"
  test -e "gitaly-bench" || git clone https://gitlab.com/gitlab-org/gitaly-bench.git
  test -x "gitaly-bench/gitaly-bench" || (cd gitaly-bench; GOOS=linux make)
  gcloud compute scp --project=t4cc0re --zone us-east1-b ./gitaly-bench/gitaly-bench $1:
fi

echo "$1 / IP: ${IP} / Creds: root / ${ADMIN_PASS}"
