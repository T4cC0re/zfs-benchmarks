#!/usr/bin/env bash

gcloud compute ssh --project=t4cc0re --zone us-east1-b $1 <<EOF
if [ "$2" = "on" ]; then
  echo 0 | sudo tee /sys/module/zfs/parameters/l2arc_noprefetch &>/dev/null
  # Based on tested write performance
  echo 419430400 | sudo tee /sys/module/zfs/parameters/l2arc_write_max &>/dev/null
  echo 419430400 | sudo tee /sys/module/zfs/parameters/l2arc_write_boost &>/dev/null
else
  echo 1 | sudo tee /sys/module/zfs/parameters/l2arc_noprefetch &>/dev/null
  echo 8388608 | sudo tee /sys/module/zfs/parameters/l2arc_write_max &>/dev/null
  echo 8388608 | sudo tee /sys/module/zfs/parameters/l2arc_write_boost &>/dev/null
fi
tail /sys/module/zfs/parameters/l2arc_noprefetch /sys/module/zfs/parameters/l2arc_write_max /sys/module/zfs/parameters/l2arc_write_boost
EOF
