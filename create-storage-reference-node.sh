gcloud compute --project=t4cc0re instances create candicate-reference-n1-standard-32 \
--zone=us-east1-b \
--machine-type=n1-standard-32 \
--subnet=default \
--network-tier=PREMIUM \
--maintenance-policy=MIGRATE \
--service-account=473940073259-compute@developer.gserviceaccount.com \
--scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append \
--image=ubuntu-1604-xenial-v20190530c \
--image-project=ubuntu-os-cloud \
--boot-disk-size=30GB \
--boot-disk-type=pd-ssd \
--boot-disk-device-name=reference-n1-standard-32-boot \
--create-disk=mode=rw,auto-delete=yes,size=16000,type=projects/t4cc0re/zones/us-east1-b/diskTypes/pd-ssd,name=reference-n1-standard-32-data \
--tags=http-server,https-server

sleep 30

./install.sh candicate-reference-n1-standard-32 ext4
